/* Création de la table des compléments CNSA - Données contenues :
 * [X] HEB_IS_HAB_AIDE_SOC
 * [X] HEB_CAPA_TOTALE
 * [X] HEB_CAPA_COMPLET
 * [X] HEB_CAPA_TEMPO
 * [X] HEB_CAP_ACC_JOUR
 * [X] HEB_CAPA_HAS
 * [X] HEB_CAPA_UHR
 * [X] HEB_CAP_LOG_F1
 * [X] HEB_CAP_LOG_F2
 * [X] HEB_CAP_LOG_F1_BIS
 * [X] SRV_CAPA_TOTALE
 */ 
create table COMPLEMENT_CNSA_BATCH (
   NOFINESSET           varchar(9)           not null,
   CODE_TYPE_ATTRIBUT_CNSA varchar(64)          not null,
   VALEUR               varchar(1024)         not null,
   SEP_DATEMAJ          datetime             not null,
   SEP_DATESUP          datetime             null,
   constraint PK_COMPLEMENT_CNSA_BATCH primary key (NOFINESSET, CODE_TYPE_ATTRIBUT_CNSA)
);
