UPDATE STATUT_JURIDIQUE
SET STATUT_GRAND_PUBLIC='PUBLIC'
WHERE CODE_PARENT LIKE '11__'
OR CODE_PARENT LIKE '12__'
OR CODE='1100'
OR CODE='1200';

UPDATE STATUT_JURIDIQUE
SET STATUT_GRAND_PUBLIC='PRIVE_NON_LUCRATIF'
WHERE CODE_PARENT LIKE '21__'
OR CODE='2100';

UPDATE STATUT_JURIDIQUE
SET STATUT_GRAND_PUBLIC='PRIVE_COMMERCIAL'
WHERE CODE_PARENT LIKE '22__'
OR CODE='2200';

UPDATE STATUT_JURIDIQUE
SET STATUT_GRAND_PUBLIC='DROIT_ETRANGER'
WHERE CODE_PARENT LIKE '31__'
OR CODE='3100';
