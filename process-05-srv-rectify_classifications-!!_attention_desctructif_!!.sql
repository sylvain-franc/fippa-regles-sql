/* ESA PPA est propre au service, nul besoin de filtrer sur les services via la classification
 */
DELETE FROM CLASSIFICATION_ETABLISSEMENT
WHERE CODE_CLASSIFICATION='ESA_PPA'
AND NOFINESSET NOT IN
	(SELECT NOFINESSET FROM EQUIPEMENT_SOCIAL_OU_MEDICO_SOCIAL
	WHERE CLIENT='436'
	AND DE='357');
