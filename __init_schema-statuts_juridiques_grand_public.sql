alter table STATUT_JURIDIQUE
add STATUT_GRAND_PUBLIC varchar(24) null;

create table STATUT_JURIDIQUE_GRAND_PUBLIC (
   CODE                 varchar(24)          not null,
   LIBELLE              varchar(60)          null,
   LIBELLE_COURT        varchar(20)          not null,
   DATEDEBUT            datetime             not null,
   DATEFIN              datetime             null
constraint PK_STATUT_JURIDIQUE_GRAND_PUBLIC primary key (CODE)
);

alter table STATUT_JURIDIQUE
   add constraint FK_STATUT_JURIDIQUE_STATUT_JURIDIQUE_GRAND_PUBLIC foreign key (STATUT_GRAND_PUBLIC)
      references STATUT_JURIDIQUE_GRAND_PUBLIC (CODE);
